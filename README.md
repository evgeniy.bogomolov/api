**Api**

У 1 клиента может быть несколько телефонов и заявок, но только 1 паспортные данные.

У нескольких клиентов могут быть одинаковые телефоны (но в базе это все равно будет 1 запись).

Из-за того, что номера телефонов могут менять владельцев (а владельцы могут менять номера) - они привязаны не к клиентам, а к заявкам.

Может быть несколько клиентов с разными ФИО и одинаковыми паспортными данными (если человек ошибся при вводе фио и ошибки нет в правилах замены, то мы не можем определить какая именно заявка ошибочная и оставляем все)

Уникальность клиента проверяется по хэшу фио+паспорт.

При отправке формы подразумевается, что с каждой частью данных будет идти уникальный ключ, по которому связываем части заявки.

**Замены.**

Замена работает только по имени. Если после запуска замены получается 2 записи с одинаковыми ФИО+паспорт, то ошибочный клиент удаляется, а его заявки привязываются к правильному.