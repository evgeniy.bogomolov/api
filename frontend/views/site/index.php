<?php

/* @var $this yii\web\View */

use common\models\Bid;
use yii\bootstrap\ActiveForm;

$this->title = 'My Yii Application';
?>
<div class="site-index">

	<?php $form = ActiveForm::begin([
	        'method' => 'post',
            'action' => '/admin/api/add-bid'
    ]); ?>

    <div class="step">
        <?= $form->field($bid, 'site_id')->textInput() ?>
        <?= $form->field($client, 'name')->textInput() ?>
        <?= $form->field($client, 'surname')->textInput() ?>
        <?= $form->field($client, 'patronymic')->textInput() ?>
	    <?= $form->field($phone, 'number')->textInput() ?>
        <?= $form->field($bid, 'unique_key')->textInput() ?>

        <a href="#" class="step__send">Отправить первую часть по AJAX</a>
    </div>

    <div class="step">
	    <?= $form->field($passport, 'series')->textInput() ?>
	    <?= $form->field($passport, 'number')->textInput() ?>
	    <?= $form->field($bid, 'unique_key')->textInput() ?>

        <a href="#" class="step__send">Отправить вторую часть по AJAX</a>
    </div>

	<?php ActiveForm::end(); ?>
</div>