<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sites".
 *
 * @property int $id
 * @property string $name
 */
class Site extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sites';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID сайта',
            'name' => 'Имя сайта',
        ];
    }
}
