<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "passports".
 *
 * @property int $id
 * @property int $series
 * @property int $number
 * @property int $client_id
 * @property string $created_at
 * @property string $updated_at
 */
class Passport extends \yii\db\ActiveRecord
{
	/**
	 * {@inheritdoc}
	 */
	public function behaviors()
	{
		return [
			[
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
				'value' => new Expression('NOW()'),
			],
		];
	}

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'passports';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['series', 'number', 'client_id'], 'integer'],
            [['series'], 'string', 'length'=>4],
            [['number'], 'string', 'length'=>6],
	        [['series', 'number'], 'unique', 'targetAttribute' => ['series', 'number']],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'series' => 'Series',
            'number' => 'Number',
            'client_id' => 'Client ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
