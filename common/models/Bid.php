<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "bids".
 *
 * @property int $id
 * @property int $client_id
 * @property int $phone_id
 * @property int $passport_id
 * @property int $site_id
 * @property string $unique_key
 * @property string $created_at
 * @property string $updated_at
 * @property object $client
 */
class Bid extends \yii\db\ActiveRecord
{
	/**
	 * {@inheritdoc}
	 */
	public function behaviors()
	{
		return [
			[
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
				'value' => new Expression('NOW()'),
			],
		];
	}

	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {
			if($this->client_id && $this->passport_id) {
				$this->client->passport_id = $this->passport_id; //привязываем паспорт к клиенту

				$existing_client = Client::find()->where(['hash' => $this->client->generateHash()])->one();

				if($existing_client) { //если клиент с таким фио и паспортом уже есть
					Client::findOne($this->client->id)->delete(); //удаляем дубль клиента
					$this->client_id = $existing_client->id; //привязываем к заявке добавленного ранее клиента
				} else {
					$this->client->save();
				}
			}

			return true;
		} else {
			return false;
		}
	}

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'bids';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['client_id', 'phone_id', 'passport_id', 'site_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['unique_key'], 'string'],
            [['unique_key'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'client_id' => 'Client ID',
            'phone_id' => 'Phone ID',
            'passport_id' => 'Passport ID',
            'site_id' => 'Site ID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

	public function getClient() {
		return $this->hasOne( Client::className(), [ 'id' => 'client_id' ] );
	}

	public function getPhone() {
		return $this->hasOne( Phone::className(), [ 'id' => 'phone_id' ] );
	}

	public function getPassport() {
		return $this->hasOne( Passport::className(), [ 'id' => 'passport_id' ] );
	}

	public function getSite() {
		return $this->hasOne( Site::className(), [ 'id' => 'site_id' ] );
	}
}
