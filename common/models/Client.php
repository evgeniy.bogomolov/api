<?php

namespace common\models;

use backend\models\Replacement;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "clients".
 *
 * @property int $id
 * @property string $name
 * @property string $surname
 * @property string $patronymic
 * @property string $fullname
 * @property int $passport_id
 * @property string $created_at
 * @property string $updated_id
 * @property string $hash
 * @property object $bids
 * @property object $passport
 */
class Client extends ActiveRecord
{
	/**
	 * {@inheritdoc}
	 */
	public function behaviors()
	{
		return [
			[
				'class' => TimestampBehavior::className(),
				'attributes' => [
					ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
					ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
				],
				'value' => new Expression('NOW()'),
			],
		];
	}

	public function beforeSave($insert)
	{
		if (parent::beforeSave($insert)) {
			if($this->name && $this->surname && $this->patronymic && $this->passport_id) {
				$this->hash = $this->generateHash();
			}

			$replacement = Replacement::find()->where(['before' => $this->name])->one(); //проверяем, есть ли правила замены для имени

			if($replacement) {
				$this->name = $replacement->after;
			}

			return true;
		} else {
			return false;
		}
	}

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clients';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['passport_id'], 'integer'],
            [['name', 'surname', 'patronymic'], 'string', 'max' => 255],
            [['name', 'surname', 'patronymic'], 'required'],
	        [['created_at', 'updated_at'], 'safe'],
	        [['hash'], 'string']
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'surname' => 'Surname',
            'patronymic' => 'Patronymic',
            'passport_id' => 'Passport ID',
        ];
    }

	public function getBids() {
		return $this->hasMany(Bid::className(), ['client_id' => 'id']);
	}

	public function getPassport() {
		return $this->hasOne(Passport::className(), ['id' => 'passport_id']);
	}

	public function getPhones(  ) {
		$bids = $this->bids;
		$phones = [];

		foreach ($bids as $bid) {
			$phones[] = $bid->phone->number;
		}

		return array_unique($phones);
	}

    public function getFullName() {
    	return $this->name." ".$this->surname." ".$this->patronymic;
    }

    public function generateHash() {
	    return md5($this->fullname.$this->passport_id);
    }
}
