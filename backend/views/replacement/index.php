<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Replacements';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="replacement-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Replacement', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [

            'id',
            'before',
            'after',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>

    <p>
		<?= Html::a('Выполнить замены', ['do-replacements'], ['class' => 'btn btn-success']) ?>
    </p>
</div>
