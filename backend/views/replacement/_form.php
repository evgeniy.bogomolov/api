<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model backend\models\Replacement */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="replacement-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'before')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'after')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
