<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Replacement */

$this->title = 'Create Replacement';
$this->params['breadcrumbs'][] = ['label' => 'Replacements', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="replacement-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
