<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model backend\models\Replacement */

$this->title = 'Update Replacement: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Replacements', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="replacement-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
