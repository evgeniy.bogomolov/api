<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Client */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Clients', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="client-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'surname',
            'patronymic',
	        [
		        'label'  => 'Паспорт',
		        'value'     => function ( $model ) {

			        return $model->passport->series." ".$model->passport->number;
		        },
	        ],
            'created_at',
            'updated_at',
            'hash',
	        [
		        'label'  => 'Заявки',
		        'format' => 'html',
		        'value'     => function ( $model ) {
                    $result = "";
			        foreach ( $model->bids as $bid ) {
                        $result .= "<a href=".Url::to(['bid/view', 'id' => $bid->id]).">#$bid->id</a> / ";
                    }
			        return $result;
		        },
	        ],
	        [
		        'label'  => 'Телефоны',
		        'value'     => function ( $model ) {
			        $result = "";
			        foreach ( $model->phones as $phone ) {
				        $result .= "$phone / ";
			        }
			        return $result;
		        },
	        ],
        ],
    ]) ?>

</div>
