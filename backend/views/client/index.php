<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Clients';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="client-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Client', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'name',
            'surname',
            'patronymic',
	        [
		        'label'  => 'Паспорт',
		        'value'     => function ( $model ) {

			        return $model->passport->series." ".$model->passport->number;
		        },
	        ],

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
