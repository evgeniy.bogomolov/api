<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bids';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="bid-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Bid', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
	        [
		        'attribute' => 'client_id',
		        'label'     => 'Клиент',
		        'format'    => 'html',
		        'value'     => function ( $model ) {
			        return "<a href=".Url::to(['client/view', 'id' => $model->client->id]).">".$model->client->fullname."</a>";
		        },
	        ],
	        [
		        'attribute' => 'phone_id',
		        'label'     => 'Телефон',
		        'value'     => function ( $model ) {
			        return $model->phone->number;
		        },
	        ],
	        [
		        'attribute' => 'passport_id',
		        'label'     => 'Паспорт',
		        'value'     => function ( $model ) {
			        return $model->passport->series." ".$model->passport->number;
		        },
	        ],
	        [
		        'attribute' => 'site_id',
		        'label'     => 'Сайт',
		        'value'     => function ( $model ) {
			        return $model->site->name;
		        },
	        ],
            'created_at',
            //'updated_at',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
