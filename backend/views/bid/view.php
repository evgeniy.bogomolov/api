<?php

use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\Bid */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Bids', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="bid-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
	        [
		        'attribute' => 'client_id',
		        'label'     => 'Клиент',
		        'format'    => 'html',
		        'value'     => function ( $model ) {
			        return "<a href=".Url::to(['client/view', 'id' => $model->client->id]).">".$model->client->fullname."</a>";
		        },
	        ],
	        [
		        'attribute' => 'phone_id',
		        'label'     => 'Телефон',
		        'value'     => function ( $model ) {
			        return $model->phone->number;
		        },
	        ],
	        [
		        'attribute' => 'passport_id',
		        'label'     => 'Паспорт',
		        'value'     => function ( $model ) {
			        return $model->passport->series." ".$model->passport->number;
		        },
	        ],
	        [
		        'attribute' => 'site_id',
		        'label'     => 'Сайт',
		        'value'     => function ( $model ) {
			        return $model->site->name;
		        },
	        ],
            'created_at',
            'updated_at',
        ],
    ]) ?>

</div>
