<?php

namespace backend\controllers;

use Codeception\Module\Cli;
use common\models\Bid;
use common\models\Client;
use Yii;
use backend\models\Replacement;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * ReplacementController implements the CRUD actions for Replacement model.
 */
class ReplacementController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Replacement models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Replacement::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

	/**
	 * Do replacements.
	 * @return mixed
	 */
	public function actionDoReplacements()
	{
		$replacements = Replacement::find()->all();
		$counter = 0;

		foreach ( $replacements as $replacement ) {
			$clients = Client::find()->where(['name' => $replacement->before])->all(); //находим клиентов, которые попадают под правило замены
			if($clients) {
				foreach ( $clients as $client ) {
					$counter++;
					$client->name = $replacement->after;

					$client_duplicate = Client::find()->where(['hash' => $client->generateHash()])->one(); //находим дубли клиентов
					if($client_duplicate) {
						Bid::updateAll(['client_id' => $client_duplicate->id], "client_id = $client->id"); //переносим заявки от ошибочного клиента к правильному
						$client->delete();
					} else { //если дубликатов нет, то просто меняем имя
						$client->save();
					}
				}
			}
		}

		Yii::$app->session->setFlash('success', "Проведено операций: ".$counter);

		return $this->redirect('index');
	}

    /**
     * Displays a single Replacement model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Replacement model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Replacement();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Replacement model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Replacement model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Replacement model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Replacement the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Replacement::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
