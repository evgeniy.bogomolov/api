<?php
namespace backend\controllers;

use backend\models\Replacement;
use common\models\Bid;
use common\models\Client;
use common\models\Passport;
use common\models\Phone;
use Yii;
use yii\web\Controller;
use yii\web\Response;

/**
 * Api controller
 */
class ApiController extends Controller
{
	public function beforeAction($action)
	{
		$this->enableCsrfValidation = false;

		return parent::beforeAction($action);
	}

    public function actionAddBid()
    {
		$bid = new Bid();
		$client = new Client();
		$phone = new Phone();
		$passport = new Passport();
	    $errors = false;

		$bid->load(Yii::$app->request->get());

		$existing_bid = Bid::find()->where(["unique_key" => $bid->unique_key])->one();

		if($existing_bid) {
			$bid = $existing_bid;
		}

    	if($client->load(Yii::$app->request->get())) {
			if(!$client->save()) {
				$errors[] = $client->errors;
			}

		    $bid->client_id = $client->id;
	    }

	    if($phone->load(Yii::$app->request->get())) {
			if(!$phone->save()) {
				$phone = Phone::find()->where(['number' => $phone->number])->one(); //Если телефон уже есть в базе, то берем его
			}

		    $bid->phone_id = $phone->id;
	    }

	    if($passport->load(Yii::$app->request->get())) {
			if(!$passport->save()) {
				$passport = Passport::find()->andWhere(['series' => $passport->series, 'number' => $passport->number])->one(); //Если паспорт уже есть в базе, то берем его
			}

		    $bid->passport_id = $passport->id;
	    }

	    if(!$bid->save()) {
		    $errors[] = $bid->errors;
	    }

		if($errors) {
			Yii::$app->response->format = Response::FORMAT_JSON;
			return $errors;
		} else {
			return "OK";
		}
    }
}
