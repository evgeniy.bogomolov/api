<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "replacements".
 *
 * @property int $id
 * @property string $before
 * @property string $after
 */
class Replacement extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'replacements';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['before', 'after'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'before' => 'До',
            'after' => 'После',
        ];
    }
}
