<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%replacements}}`.
 */
class m200301_165026_create_replacements_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%replacements}}', [
            'id' => $this->primaryKey(),
            'before' => $this->string(),
            'after' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%replacements}}');
    }
}
