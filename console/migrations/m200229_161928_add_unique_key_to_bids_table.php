<?php

use yii\db\Migration;

/**
 * Class m200229_161928_add_unique_key_to_bids_table
 */
class m200229_161928_add_unique_key_to_bids_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
	    $this->addColumn('bids', 'unique_key', $this->string());
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        echo "m200229_161928_add_unique_key_to_bids_table cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200229_161928_add_unique_key_to_bids_table cannot be reverted.\n";

        return false;
    }
    */
}
