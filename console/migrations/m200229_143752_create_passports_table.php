<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%passports}}`.
 */
class m200229_143752_create_passports_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%passports}}', [
            'id' => $this->primaryKey(),
            'series' => $this->integer(),
            'number' => $this->integer(),
            'client_id' =>$this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%passports}}');
    }
}
