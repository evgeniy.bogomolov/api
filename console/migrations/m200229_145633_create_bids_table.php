<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%bids}}`.
 */
class m200229_145633_create_bids_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%bids}}', [
            'id' => $this->primaryKey(),
			'client_id' => $this->integer(),
			'phone_id' => $this->integer(),
			'passport_id' => $this->integer(),
            'site_id' => $this->integer(),
            'created_at' => $this->dateTime(),
            'updated_at' => $this->dateTime(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%bids}}');
    }
}
